import {Component, EventEmitter, Input, Output} from '@angular/core';
import {OneItem} from "../shared/oneItem.model";

@Component({
  selector: 'app-one-item',
  templateUrl: './one-item.component.html',
  styleUrls: ['./one-item.component.css']
})
export class OneItemComponent {
  @Input() oneItem!: OneItem;
  @Output() give = new EventEmitter();


  giveItem(event:Event) {
    this.give.emit();
  }
}
