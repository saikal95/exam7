export class OneItem{
  constructor(
    public price : number,
    public name : string,
    public icon: string,
    public count: 0,
  ) {
  }
}
